# README #

This is the isocreator wrapper for all installers based on OpenBSD like
SpamCheetah

### What is this repository for? ###

* Creating ISO installer from raw VM on disk
* You should give the raw VM without the *.img* extension and 
  ISO Output name without the *.iso* extension to *./createLiveCD*
script

### How do I get set up? ###

* The tool is to invoked as root
* The img is to be xz and it should exist as <name>.img.xz
* the imgiso/ directory and mnt/ directory are supposed to be empty and
 is used for ISO creation
* The important files are under scratch directory

* the ulle.img.gz VM raw file contains the ISO creation VM

* This tool only works under OpenBSD

### Who do I talk to? ###

* Girish V
* girish@gayatri-hitech.com
